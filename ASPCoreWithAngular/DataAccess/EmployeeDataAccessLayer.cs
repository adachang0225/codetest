﻿using ASPCoreWithAngular.Interfaces;
using ASPCoreWithAngular.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
namespace ASPCoreWithAngular.DataAccess
{
    public class EmployeeDataAccessLayer : IEmployee
    {
        private string connectionString;
        public string path = @"./ClientApp/src/assets/namelists.json";
        public EmployeeDataAccessLayer(IConfiguration configuration)
        {
            connectionString = configuration["ConnectionStrings:DefaultConnection"];
        }

        //To View all employees details
        public IEnumerable<Employee> GetAllEmployees()
        {
            try
            {


                return getAllEmployeeList();

            }
            catch
            {
                throw;
            }
        }


        public List<Employee> getAllEmployeeList() {
            List<Employee> emList = new List<Employee>();
            string json = File.ReadAllText(path);
          if (json.Contains("},{"))
            {
                emList = JsonConvert.DeserializeObject<List<Employee>>(json);

            }
            else
            {
                emList.Add(JsonConvert.DeserializeObject<Employee>(json));

            }
            return emList;
        }
        public int FindMaxValue<T>(List<T> list, Converter<T, int> projection)
        {
            if (list.Count == 0)
            {
                throw new InvalidOperationException("Empty list");
            }
            int maxValue = int.MinValue;
            foreach (T item in list)
            {
                int value = projection(item);
                if (value > maxValue)
                {
                    maxValue = value;
                }
            }
            return maxValue;
        }
        //To Add new employee record 
        public int AddEmployee(Employee employee)
        {
            try
            {
                
              
                if (File.Exists(path))
                {
                    List<Employee> emList = getAllEmployeeList();
                    int maxID = FindMaxValue(emList, delegate (Employee x) { return x.EmployeeId; });
                    employee.EmployeeId = maxID+1;
                    emList.Add(employee);

                    WriteIntoJson(emList);





                }
                else if(!File.Exists(path)) {
string JsonResult = JsonConvert.SerializeObject(employee);
                    using (var tw = new StreamWriter(path, true)) {
                        tw.WriteLine(JsonResult.ToString());
                        tw.Close();
                    }
                }
                return 1;

            }
            catch
            {
                throw;
            }
        }
        public void WriteIntoJson(List<Employee> emList) {
            string JsonResult = JsonConvert.SerializeObject(emList);

            File.Delete(path);
            using (var tw = new StreamWriter(path, true))
            {
                tw.WriteLine(JsonResult.ToString());
                tw.Close();
            }

        }
        //To Update the records of a particluar employee
        public int UpdateEmployee(Employee employee)
        {
            try
            {

                if (File.Exists(path))
                {
                    List<Employee> emList = getAllEmployeeList();

                    for (int i = 0; i < emList.Count; i++) {
                        if (emList[i].EmployeeId == employee.EmployeeId)
                        {
                            emList[i].FirstName = employee.FirstName;
                            emList[i].LastName = employee.LastName;
                        }
                    }
                    WriteIntoJson(emList);

                }
                else if (!File.Exists(path))
                {
                    this.AddEmployee(employee);
                }
                return 1;
            }
            catch
            {
                throw;
            }
        }

        //Get the details of a particular employee
        public Employee GetEmployeeData(int id)
        {
            try
            {
                Employee employee = new Employee();
                if (File.Exists(path))
                {
                    List<Employee> emList = getAllEmployeeList();

                    for (int i = 0; i < emList.Count; i++)
                    {
                        if (emList[i].EmployeeId == id)
                        {
                            employee = emList[i];
                        }
                    }
               

                }
             
                return employee;
            }
            catch
            {
                throw;
            }
        }

        //To Delete the record on a particular employee
        public int DeleteEmployee(int id)
        {
            try
            {
                if (File.Exists(path))
                {
                    List<Employee> emList = getAllEmployeeList();
                    int x = 0;
                    for (int i = 0; i < emList.Count; i++)
                    {
                          if (emList[i].EmployeeId == id)
                        {
                            x = i;
                        }
                   }
                   emList.RemoveAt(x);

                    WriteIntoJson(emList);

                }
                
                return 1;
            }
            catch
            {
                throw;
            }
        }

    }
}
