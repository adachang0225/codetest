import { Component } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { Employee } from 'src/models/employee';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-employee',
  templateUrl: './fetch-employee.component.html',
  styleUrls: ['./fetch-employee.component.css']
})
export class FetchEmployeeComponent {

  public empList: Employee[];

  constructor(private httpService: HttpClient, private _employeeService: EmployeeService) {
 
    this.getEmployees();
  }

  getEmployees() {
  /* this._employeeService.getEmployees().subscribe(
     (data: EmployeeList[]) => this.empList = data
    );*/
    this.httpService.get('./assets/namelists.json').subscribe(
      data => {
        this.empList = data as EmployeeList[];	 // FILL THE ARRAY WITH DATA.
        //  console.log(this.arrBirds[1]);
      }, error => console.error(error));
    
  }

 

  delete(employeeID) {
    const ans = confirm('Do you want to delete employee with Id: ' + employeeID);
    if (ans) {
      this._employeeService.deleteEmployee(employeeID).subscribe(() => {
        this.getEmployees();
      }, error => console.error(error));
    }
  }


}

export interface EmployeeList {
  EmployeeId: number;
  FirstName: string;
  LastName: string;
}