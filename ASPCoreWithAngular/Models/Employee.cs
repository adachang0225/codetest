﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCoreWithAngular.Models
{
    public class Employee
    {
        private int employeeId;
        public int EmployeeId
        {
            get
            {
                return this.employeeId;
            }
            set
            {
                this.employeeId = value;
            }
        }
        private string firstName;

        public string FirstName {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
            }
        }
        private string lastName;

        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                this.lastName = value;
            }
        }

     
    }
}
